"""
Constants
---------

Specific constants to be used in correction, to help with consistency.
"""
# Column Names -----------------------------------------------------------------
# Pre- and Suffixe
ERR = "ERR"  # Error of the measurement
RMS = "RMS"  # Root-Mean-Square
RES = "RES"  # Rescaled measurement
DELTA = "DELTA"  # Delta between measurement and model (sometimes beating)
MDL = "MDL"  # Model
VALUE = "VALUE"
WEIGHT = "WEIGHT"
ERROR = "ERROR"
MODEL = "MODEL"
DIFF = "DIFF"

# Names
NAME = "NAME"
NAME2 = "NAME2"
S = "S"
BETA = "BET"
BETABEAT = "BB"
DISP = "D"
F1001 = "F1001"
F1010 = "F1010"
INCR = "incr"
NORM_DISP = "ND"
PHASE = "PHASE"
PHASE_ADV = "MU"
TUNE = "Q"
